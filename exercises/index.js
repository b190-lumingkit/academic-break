import {detail} from './other.js';


// Exercise 1
console.log("Exercise 1: Recipe Card.");

let recipe = {
    title: "Mango Float",
    servings: 8,
    ingredients: ["5 ripe mangoes", "960ml all-purpose cream", "1 can condensed milk", "1/2 teaspoon kosher salt", "21 graham crackers"]
}

console.log(`${recipe.title}\nServes: ${recipe.servings}\nIngredients:`);
recipe.ingredients.forEach(el => console.log(el));

// Exercise 2
console.log("Exercise 2: Reading List.");
function Book(title, author, alreadyRead) {
    this.title = title;
    this.author = author;
    this.alreadyRead = alreadyRead;
}

let library = [
    new Book("The Hunger Games", "Suzanne Collins", false),
    new Book("Percy Jackson and the Olympians Series", "Rick Riordan", true),
    new Book("The Judge's List", "John Grisham", false)
]

library.forEach(book => {
    if (book.alreadyRead) {
        console.log(`You already read "${book.title}" by ${book.author}.`);
    } else {
        console.log(`You still need to read "${book.title}" by ${book.author}`)
    }
})

// ES6 practice

console.warn(`ES6 Practice`);

const contacts = {
    firstName: 'John',
    familyName: 'Doe',
    age: 24
}

let {firstName, familyName, age} = contacts;
console.log(firstName);
console.log(familyName);
console.log(age);

// renaming variables when destructing arrays and objects
let {firstName:myName, familyName:myLastName, age:myAge} = contacts;
console.log(myName);
console.log(myLastName);
console.log(myAge);

//assigning variables to array elements
let [book1, book2, book3] = library;
console.log(book1)
console.log(book2)
console.log(book3)

// import and export modules
console.log(detail('Jaycee', 24));

