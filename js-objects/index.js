function Character(name, role, level) {
    this.name = name;
    this.role = role;
    this.level = level;
    this.health = 4*this.level;
    this.attack = 2*this.level;
    this.mana = Math.round(1.5*(this.role !== 'priest') ? this.level : 0.5*this.health);
    this.defense = Math.round(0.5*(this.role !== 'priest') ? this.level : this.health);
    this.abilities = {
        ultimate: {
            name: "Character Ultimate",
            damage: (this.role !== 'priest') ? 2*this.attack : 0.9*this.health,
            manacost: 0.5*this.mana,
            cooldown: 60,
        },
        skill1: {
            name: "Basic Skill",
            damage: (this.role !== 'priest') ? 1.5*this.attack : 0.2*this.health,
            manacost: 0.1*this.mana,
            cooldown: 10
        }
    },
    this.cast = function(ability, target) {
        if (typeof ability === 'string' && typeof target === 'object') 
        {
            skill = this.abilities[ability].name;
            damage = this.abilities[ability].damage;
            manacost = this.abilities[ability].manacost;
            
            if (this.mana - manacost <= 0) {
                console.warn("You don't have any mana left.");
            } else {
                totalDamage = target.defense - damage;
                target.health = Math.round(target.health - Math.abs(totalDamage));
                this.mana = Math.round(this.mana - manacost);
                console.warn(`${this.name} cast "${skill}" to ${target.name}`);
                console.log(`${target.name}'s health is reduced to ${target.health}`);
                console.log(`${this.name}'s mana is down to ${this.mana}`);

                if (target.health <= 0) {
                    target.died();
                }
            }
        } 
        else 
        {
            console.warn("Skill invalid.");
        }
    }
    this.died = function() {
        console.log(`${this.name} fainted.`);
    }
}

let priest= new Character('John the IV', 'priest', 12);
let assassin = new Character('Jack the Ripper', 'assassin', 15);

console.log(priest);
console.log(assassin);
priest.cast('ultimate', assassin);
assassin.cast('ultimate', priest);
assassin.cast('ultimate', priest);





