function Book(author, title, datePublished, summary){
    this.author = author;
    this.title = title;
    this.datePublished = datePublished;
    this.summary = summary;
}

let book1 = new Book("John Green", "Looking for Alaska", "March 3, 2005", "Miles “Pudge” Halter is done with his safe life at home. He heads off to the sometimes crazy and anything-but-boring world of Culver Creek Boarding School, and his life becomes the opposite of safe.", );

let book2 = new Book("J.K. Rowling", "Harry Potter and the Deathly Hallows", "July 14, 2007", "It's no longer safe for Harry at Hogwarts, so he and his best friends, Ron and Hermione, are on the run. At the same time, their friendship, fortitude, and sense of right and wrong are tested in ways they never could have imagined.");

let book3 = new Book("Rick Riordan", "Percy Jackson & the Olympians: The Titan's Curse", "May 1, 2007", "An ancient monster has arisen — one rumored to be so powerful it could destroy Olympus — and Artemis, the only goddess who might know how to track it, is missing. Now Percy and his friends, along with the Hunters of Artemis, have only a week to find the kidnapped goddess and solve the mystery of the monster she was hunting.");


console.log(`Book 1:`)
console.log(
    `Author: ${book1.author}\nTitle: ${book1.title}\nDate Published: ${book1.datePublished}\nSummary: ${book1.summary}`
);

console.log(`Book 2:`)
console.log(
    `Author: ${book2.author}\nTitle: ${book2.title}\nDate Published: ${book2.datePublished}\nSummary: ${book2.summary}`
);

console.log(`Book 3:`)
console.log(
    `Author: ${book3.author}\nTitle: ${book3.title}\nDate Published: ${book3.datePublished}\nSummary: ${book3.summary}`
);



